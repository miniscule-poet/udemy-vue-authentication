describe('The Home Page', () => {
    it('successfully loads', () => {
        cy.visit('/')

        cy.get('.md-headline').contains('Welcome')

        cy.get('title').contains('authentication')

    })

})
