/* This test needs to have a proper DB setup/teardown method after registering */

beforeEach(() => {
    cy.visit('/register')
})

describe('The Register Page', () => {
    const currentUser = {
        firstName: 'Tester',
        lastName: 'Test',
        email: 'test@test.de',
        age: '10',
        password: '123456'
    }

    it('should get an error message, when one required input is empty', () => {

        cy.get('input[name=first-name')
            .focus().blur().parent().should('have.class', 'md-invalid')

    })

    it('should get an error message, when email address is already taken', () => {

        cy.get('input[name=first-name')
            .type(currentUser.firstName)

        cy.get('input[name=last-name')
            .type(currentUser.lastName)

        cy.get('input[name=email')
            .type(currentUser.email)
            .blur().parent().should('have.class', 'md-invalid')

    })

    it('should get an error message when age is below 18', () => {
        cy.get('input[name=age')
            .type(currentUser.age)
            .blur().parent().should('have.class', 'md-invalid')
    })

    it('should be able to register', () => {

        cy.server()           // enable response stubbing

        cy.route({
            method: 'POST',      // Route POST requests
            url: 'https://identitytoolkit.googleapis.com/v1/*',    // that have a URL that matches '...'
            response: {
                "kind": "identitytoolkit#SignupNewUserResponse",
                "idToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6ImM4NDI1Zjk0YmVlMWU0N2YxNGQ3OWZhYmQ4MTIyNTRiMWJmOTE4YzAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdnVlanMtaHR0cC0xZmRkMyIsImF1ZCI6InZ1ZWpzLWh0dHAtMWZkZDMiLCJhdXRoX3RpbWUiOjE1NjcwNjgzNDQsInVzZXJfaWQiOiJFb3MySXViejkwVDRqWDFnTm5XWWlaekF6UnEyIiwic3ViIjoiRW9zMkl1Yno5MFQ0algxZ05uV1lpWnpBelJxMiIsImlhdCI6MTU2NzA2ODM0NCwiZXhwIjoxNTY3MDcxOTQ0LCJlbWFpbCI6ImJsYUB0ZXN0LmRlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbImJsYUB0ZXN0LmRlIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.SS0HAZazDSOgnJ1_xFd1kg1bLMnNGiPBz-Uesd4bpoWctHRN4_QmMdyZZWvi7UgFIH0xmIpnLaSt_qioiCEYC_d3itx_p2MezybvkwSnqLidkEWLRspssTs0M4spTpOEVexK94bt7p-vttPTAB8NxJ3yyN6YS4MYU6KSjrUFkr9tZQfpFNpJU7hOTuoKGj43B32YTcwPJElF8N7irFUpw_amrPXX4THtw4FUBT6arZBkhvgqJQFZNXAAotXmMVDtAXfUOlcQPtOLwvujcAtP49hYyW2RuPM8jK4WAu_fcvq8s7PCUmaqFmbLAQIlDqp1b8lFqTK4nDMMtDJ9N83G5g",
                "email": "different@test.de",
                "refreshToken": "AEu4IL0vdrgyiENBtkazd4mOmypaXApm6ET2INd7iwt4dWwlwALMPvWlA5w9lLb_GLF7JfwRdX6kA7KZBcKeqpLY0pIC-2Ilvwbkjeel2IQIHC9CDZvxrdLoBgEvzZDpQQFIWF7ONG5qzddXFjhTmjQzvdMlF0CFIU4zF4qtepLf0Vw_3qtiSbMZvSvL3mY1VVsoiTH8QzypL7D4uYs_DrF0PuuzaQcioQ",
                "expiresIn": "3600",
                "localId": "Eos2Iubz90T4jX1gNnWYiZzAzRq2"
            }
        })

        cy.route('GET', 'https://vuejs-http-1fdd3.firebaseio.com/*', '{}')

        cy.route('POST', 'https://vuejs-http-1fdd3.firebaseio.com/*', '{"name":"-LnRYKCy7_o_sM9tw0Tp"}')

        cy.get('input[name=first-name')
            .type(currentUser.firstName)

        cy.get('input[name=last-name')
            .type(currentUser.lastName)

        cy.get('input[name=email')
            .type('different@test.de')

        cy.get('input[name=age')
            .type('40')

        cy.get('input[name=password')
            .type(currentUser.password)

        cy.get('input[name=c_password')
            .type(currentUser.password)

        cy.get('button[type=submit')
            .should('not.be.disabled').click()

    })

})

