import Vue from "vue";
import Vuex from "vuex";
import axios from './axios-auth';
import globalAxios from 'axios';
import router from './router';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  getters: {
    user (state) {
      return state.user;
    },
    isAuthenticated (state) {
      return state.idToken !== null;
    }
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token;
      state.userId = userData.userId;
    },
    currentUser (state, user) {
      state.user = user;
    },
    clearAuthData (state) {
      state.idToken = null;
      state.userId = null;
    }
  },
  actions: {
    setLogoutTimer ({dispatch}, expirationTime) {
      setTimeout(() => {
        dispatch('logout');
      }, expirationTime * 1000)
    },
    register ({commit, dispatch}, authData) {
      axios.post('/accounts:signUp?key=AIzaSyAkLfE2LiOOtb9ShBCY5XVCc0jW8SPHRNw', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
    })
        .then(result => {
            console.log('Result', result);
            commit('authUser', {
              token: result.data.idToken,
              userId: result.data.localId
            });
            dispatch('storeUser', authData);
            dispatch('setLogoutTimer', result.data.expiresIn);
            // this.lastUser = formData.firstName + ' ' + formData.lastName;
        })
        .catch(error => console.log('Error: ', error));
    },
    login ({commit, dispatch}, authData) {
      axios.post('/accounts:signInWithPassword?key=AIzaSyAkLfE2LiOOtb9ShBCY5XVCc0jW8SPHRNw', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
    })
        .then(result => {
            console.log('Result', result);
            commit('authUser', {
              token: result.data.idToken,
              userId: result.data.localId
            });
            const now = new Date();
            const expirationDate = new Date(now.getTime() + result.data.expiresIn * 1000);
            localStorage.setItem('token', result.data.idToken);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', result.data.localId);
            dispatch('setLogoutTimer', result.data.expiresIn);
            router.replace('/dashboard');
        })
        .catch(error => console.log('Error: ', error));      
    },
    tryAutoLogin ({commit}) {
      const token = localStorage.getItem('token'); //console.log('tryAutologin', token);
      if (!token) {
        return;
      }
      const expirationDate = localStorage.getItem('expirationDate');
      const now = new Date();
      if (now >= expirationDate) {
        return
      }
      const userId = localStorage.getItem('userId');
      commit('authUser', {
        token: token,
        userId: userId
      })
    },
    logout ({commit, state}) {
      commit('clearAuthData');
      localStorage.removeItem('token');
      localStorage.removeItem('userId');
      localStorage.removeItem('expirationDate');
      state.user = null;
      router.replace('/login');
    },
    storeUser ({commit, state}, userData) {
      if (!state.idToken) {
        return
      }
      globalAxios.post('/users.json' + '?auth=' + state.idToken, userData)
        .then(res => {
          console.log('Res', res);
        })
        .catch(error => console.log('Error: ', error));  
    },
    fetchUser ({commit, state}) { console.log('Fetching user...', state.idToken);
      if (!state.idToken) {
        return
      }
      globalAxios.get('/users.json' + '?auth=' + state.idToken)
        .then(res => {
          console.log('Response:', res);
          const data = res.data;
          const users = [];
          for (let key in data) {
            const user = data[key];
            user.id = key;
            users.push(user);
          }
          console.log('Users:', users);
          commit('currentUser', users[0]);
        })
        .catch(error => console.log('Error:', error))      
    }
  }
});
