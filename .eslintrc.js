module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: ["plugin:vue/essential"],

  rules: {
    // we should always disable console logs and debugging in production
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
  },

  parserOptions: {
    parser: "babel-eslint"
  }
};
