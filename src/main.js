import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from 'axios';
import vuelidate from 'vuelidate';
import { MdApp, MdToolbar, MdContent, MdTabs, MdCard, MdButton, MdField, MdSnackbar, MdLayout, MdMenu, MdProgress, MdEmptyState, MdIcon, MdAvatar } from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css';

// import 'vue-material/dist/theme/default.css';

axios.defaults.baseURL = 'https://vuejs-http-1fdd3.firebaseio.com';
// axios.defaults.headers.get['Authorization'] = 'whatever';
/* axios.interceptors.request.use(config => {
  console.log('Request interceptor', config);
  return config;
});
axios.interceptors.response.use(res => {
  console.log('Response interceptor', res);
  return res;
}); */

Vue.config.productionTip = false;
Vue.use(MdApp);
Vue.use(MdToolbar);
Vue.use(MdContent);
Vue.use(MdTabs);
Vue.use(MdCard);
Vue.use(MdButton);
Vue.use(MdField);
Vue.use(MdSnackbar);
Vue.use(MdLayout);
Vue.use(MdMenu);
Vue.use(MdProgress);
Vue.use(MdEmptyState);
Vue.use(MdIcon);
Vue.use(MdAvatar);

Vue.use(vuelidate);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

// Cypress automatically sets window.Cypress by default
if (window.Cypress) {
  window.__store__ = store
}
