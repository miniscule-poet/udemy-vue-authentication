/// <reference types="cypress" />
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("doLogin", () => {

    cy.request({
        method: 'POST',
        url: 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAkLfE2LiOOtb9ShBCY5XVCc0jW8SPHRNw',
        body: '{"email":"test@test.de","password":"123456","returnSecureToken":true}'
    })
        .then((response) => {
            const now = new Date();
            const expirationDate = new Date(now.getTime() + response.body.expiresIn * 1000);
            localStorage.setItem('token', response.body.idToken);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', response.body.localId);

        })

})

Cypress.Commands.add("doFetchUser", () => {

    let user = {"email":"test@test.de","firstName":"Tester","lastName":"Test","password":"123456"}

    cy.window().should('have.property', '__store__')

    cy.window().then( win => {
        win.__store__.commit('currentUser', user)
    })

})
