describe('The Dashboard Page', () => {

    context('Without login', () => {
        it('redirects if not logged in', () => {
            cy.visit('/dashboard')

            cy.location('pathname').should('eq', '/login')

        })
    })

    context('Needs login', () => {
        beforeEach(() => {
            cy.doLogin()

            cy.visit('/')

            cy.get('.md-tabs-navigation a').contains('Dashboard').click() // to invoke the fetch user function on created()
        })

        it('should have a headline', () => {
            cy.get('h1').contains('This is the dashboard view')
        })


        it('should show the users email address', () => {
            cy.wait(2000) // for fetching the users infos

            cy.get('p').contains('test@test.de')
        })
    })

})
