
describe('The Login Page', () => {
    const currentUser = {
        username: 'test@test.de',
        password: '123456'
    }

    beforeEach(() => {
        cy.visit('/login')
    })

    it('should have a title and a link to the registration page', () => {

        cy.contains('.md-title', 'Login')

        cy.contains('Register here').should('have.attr', 'href',  '/register')
    })

    it('should get an error message, when one input is empty', () => {

        cy.get('input[name=email')
            .type(currentUser.username)

        cy.get('input[name=password')
            .focus().parent().should('have.class', 'md-focused')

        cy.get('input[name=password')
            .focus().blur().parent().should('have.class', 'md-invalid')

        cy.get('button[type=submit')
            .should('be.disabled')
    })

    it('should log the user in and redirect to dashboard', () => {

        cy.get('input[name=email')
            .type(currentUser.username).should('have.value', 'test@test.de')

        cy.get('input[name=password')
            .type(currentUser.password).should('have.value', '123456')

        cy.get('button[type=submit')
            .should('not.be.disabled')
            .click()

        cy.url().should(() => {
            expect('include', '/dashboard')
            expect(localStorage.getItem('token')).to.not.equal(null)
        })

        cy.get('.user-menu').should('contain', 'Tester')
    })

    it('should logout the user', () => {

        cy.doLogin()

        cy.visit('/')

        cy.get('.md-button').contains('Logout').click()

        cy.url().should(() => {
            expect('include', '/login')
            expect(localStorage.getItem('token')).to.be.null
        })
    })
})

