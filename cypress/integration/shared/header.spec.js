describe('The Header element', () => {
    context('Without login', () => {
        beforeEach(() => {
            cy.visit('/')
        })

        it('should show the title', () => {
            cy.get('.md-title').contains('Authentication')
        })

        it('should have a navigation', () => {
            cy.get('.md-tabs .md-tabs-navigation')
                .children('a').eq(0).should('have.class', 'md-active')
        })

        it('should have a link to the "About" page', () => {
            cy.get('.md-tabs .md-tabs-navigation')
                .children('a').eq(2).should('have.attr', 'href', '/about')
        })
    })

    context('Needs login', () => {
        beforeEach(() => {

            cy.doLogin()

            cy.visit('/')

            cy.doFetchUser()

            //cy.get('.md-tabs-navigation a').contains('Dashboard').click() // to invoke the fetch user function on created()
        })

        it('should show the name of the user', () => {

            cy.get('.user-menu').should('contain', 'Tester')

        })

        it('should show a user card', () => {

            cy.get('.user-menu').click()

            cy.get('.author-card')
                .children().should('have.class', 'md-avatar')

            cy.get('.author-card').children()
                .should('have.class', 'author-card-info')
                .should('contain', 'Tester Test')
                .children().should('have.class', 'author-card-links')
                .children().should('have.attr', 'href', 'test@test.de')

        })

        it('should have the possibility to logout', () => {

            cy.get('.md-button').contains('Logout').click()

            cy.get('.md-button').contains('Login')

        })
    })
})

